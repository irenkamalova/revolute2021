package com.revolute;

import org.junit.Test;

public class LoadBalancerTest {

    @Test
    public void acceptOneInstance() {
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.register("url1");
    }

    @Test(expected = IllegalStateException.class)
    public void acceptOnly10Instances() {
        LoadBalancer loadBalancer = new LoadBalancer();
        for (int i = 0; i < 10; i++) {
            loadBalancer.register(String.valueOf(i));
        }
    }

    @Test(expected = IllegalStateException.class)
    public void acceptOnly10InstancesAndNoMore() {
        LoadBalancer loadBalancer = new LoadBalancer();
        for (int i = 0; i < 10; i++) {
            loadBalancer.register(String.valueOf(i));
        }
        loadBalancer.register("url1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void acceptTheSameUrl() {
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.register("url1");
        loadBalancer.register("url1");
    }
}
