package com.revolute;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MainTest {

    @Test
    public void testAnswer() {
        Main main = new Main();
        assertEquals(42, main.answer());
    }

    @Test(expected = IllegalStateException.class)
    public void testQuestion() {
        Main main = new Main();
        main.question();
    }
}
