package com.revolute;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 Load balancer

 Register instances
 Register instances to the Load Balancer – the max number of accepted backend-instances from the load balancer is 10.
 Each instance should have a unique address.


 Random invocation
 Develop an algorithm that,
 when invoking the Load Balancer's get() method multiple times,
 should return one backend-instance choosing between the registered ones randomly.
 */
public class LoadBalancer {

    private final Integer defaultLimit = 10;
    int counter = 0;
    private final Set<String> instances = new HashSet<>();

    public String get() {
        // random pickinng from instances
        // first - generate number based on size of set
        // skip this number of instances
        // get the instance
        Random random = new Random();
        int randomNumber = random.nextInt(instances.size());

        return instances.stream()
                .skip(randomNumber)
                .findFirst()
        .orElseThrow(() -> new ArrayIndexOutOfBoundsException("Couldn't retrive random instance"));
    }

    public void register(String instance) {
        if (instances.contains(instance)) {
            throw new IllegalArgumentException("This instance already registered");
        } else if (instances.size() < 10) {
            instances.add(instance);
        } else {
            // log
            throw new IllegalStateException("Limit of instances reached");
        }
    }


    /*
    connect:
        accepted: 9
    result:
        accepted: 10

    connect:
        accepted: 10
    result:
        coudn't register entity
        true/false


    192.12.12.12 -> 10
    192.12.12.11 -> 10
    connect:
     192.12.12.12 -> 1
     192.12.12.11 -> 2
    result:
     192.12.12.12 -> 2
     192.12.12.11 -> 2

    // limit connection 10
    // accept / not accept instances?
    // remove connection

    1) Decline connection -> instance wasn't register
    2) Put the connection to wait before we can accept it

     */


}
